<?php
require("koneksi.php");

$response = array();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $id_barang = $_POST["id_barang"];

    $perintah = "DELETE FROM barang WHERE id_barang = '$id_barang'";
    $ekseskusi = mysqli_query($konek, $perintah);
    $cek = mysqli_affected_rows($konek);

    if($cek > 0){
        $response["kode"] = 1;
        $response["pesan"] = "Data berhasil dihapus";
    }
    else{
        $response["kode"] = 0;
        $response["pesan"] = "Gagal mengahpus data";
    }

}
else{
    $response["kode"] = 0;
    $response["pesan"] = "Tidak ada Post Data";
}

echo json_encode($response);
mysqli_close($konek);
?>