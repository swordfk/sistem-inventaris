package com.sistem.inventorypersada.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.Model.register.Register;
import com.sistem.inventorypersada.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registrasi extends AppCompatActivity implements View.OnClickListener {

    ProgressDialog pDialog;
    Button btnRegistrasi;
    EditText rgsEmail, rgsPassword, rgsNama;
    String nama, email,password;
    TextView toLogin;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        rgsNama = findViewById(R.id.registerNama);
        rgsEmail = findViewById(R.id.registerEmail);
        rgsPassword = findViewById(R.id.registerPassword);
        btnRegistrasi = findViewById(R.id.btnRegistrasi);
        btnRegistrasi.setOnClickListener(this);
        toLogin = findViewById(R.id.toLogin);
        toLogin.setOnClickListener(this);
}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegistrasi:
                nama = rgsNama.getText().toString();
                email = rgsEmail.getText().toString();
                password = rgsPassword.getText().toString();

                register(nama,email,password);
                break;
            case R.id.toLogin:
                Intent intent = new Intent(this, Login.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void register(String nama, String email, String password) {
        APIRequestData ardRegister = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<Register>registerData = ardRegister.registerResponse(nama, email, password);
        registerData.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                if(response.body() !=null && response.body().getSuccess().equals("1")){
                    Toast.makeText(Registrasi.this,response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Registrasi.this, Login.class);
                    startActivity(intent);
//                    finish();
                }else {
                    Toast.makeText(Registrasi.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(Registrasi.this, Dashboard.class);
//                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                Toast.makeText(Registrasi.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}