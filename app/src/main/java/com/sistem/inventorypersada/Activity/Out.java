package com.sistem.inventorypersada.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.Adapter.AdapterGetData;
import com.sistem.inventorypersada.CRUD.CreateData;
import com.sistem.inventorypersada.Model.DataModel;
import com.sistem.inventorypersada.Model.ResponseModel;
import com.sistem.inventorypersada.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Out extends AppCompatActivity {
    private RecyclerView rvData;
    private RecyclerView.Adapter adData;
    private RecyclerView.LayoutManager lmData;
    private List<DataModel> listData = new ArrayList<>();
    private SwipeRefreshLayout srlData;
    private ProgressBar progresData;
    private FloatingActionButton fabTambah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out);

        //inisialisasi
        rvData = findViewById(R.id.outData);
        srlData = findViewById(R.id.swapData);
        progresData = findViewById(R.id.pbData);
        fabTambah = findViewById(R.id.btnCreate);

        lmData = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rvData.setLayoutManager(lmData);

        //refresh data ketika di swipe
        srlData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlData.setRefreshing(true);
                dataOut();
                srlData.setRefreshing(false);
            }
        });
        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Out.this, CreateData.class));
            }
        });
    }

    // update data ketika di close home
    @Override
    protected void onResume(){
        super.onResume();
        dataOut();
    }

    //get data barang keluar dari database
    public void dataOut(){
        progresData.setVisibility(View.VISIBLE);

        APIRequestData ardData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> tampilData = ardData.barangKeluar();

        tampilData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                //Toast.makeText(Out.this, "Kode : "+kode+" | Pesan : "+pesan, Toast.LENGTH_SHORT).show();

                listData = response.body().getData();

                adData = new AdapterGetData(Out.this, listData);
                rvData.setAdapter(adData);
                adData.notifyDataSetChanged();

                progresData.setVisibility(View.INVISIBLE);

            }

            //jika gagal terhubung ke server
            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(Out.this, "gagal menghubungi server"+t.getMessage(), Toast.LENGTH_SHORT).show();

                progresData.setVisibility(View.INVISIBLE);

            }
        });

    }
}