package com.sistem.inventorypersada.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements View.OnClickListener{
    ProgressDialog pDialog;
//    private Button btnLogin;
    private FloatingActionButton btnLogin;
    EditText emailLgn, passwordLgn;
    TextView btndaftar;
    String email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //inisialisasi
        btnLogin = (FloatingActionButton) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
//        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        emailLgn = (EditText) findViewById(R.id.emailLgn);
        passwordLgn = (EditText) findViewById(R.id.passwordLgn);
        btndaftar = findViewById(R.id.btnDaftar);
        btndaftar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                email = emailLgn.getText().toString();
                password = passwordLgn.getText().toString();
                login(email, password);
                break;
            case R.id.btnDaftar:
                Intent intent =  new Intent(this, Registrasi.class);
                startActivity(intent);
                break;
        }

    }

    private void login(String email, String password) {

        APIRequestData ardLogin = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<com.sistem.inventorypersada.Model.login.Login> loginData = ardLogin.loginResponse(email, password);
        loginData.enqueue(new Callback<com.sistem.inventorypersada.Model.login.Login>() {
            @Override
            public void onResponse(Call<com.sistem.inventorypersada.Model.login.Login> call, Response<com.sistem.inventorypersada.Model.login.Login> response) {
                if(response.body().getSuccess().equals("0")){
                    Log.i("TAG", "onXXX: ");
                    Toast.makeText(Login.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else if (response.body().getSuccess().equals("1")){
                    Toast.makeText(Login.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Login.this, Dashboard.class));
                    finish();
                }
                else {
                    Toast.makeText(Login.this, response.code() + " "+ response.message(), Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(Login.this, Dashboard.class);
//                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<com.sistem.inventorypersada.Model.login.Login> call, Throwable t) {
                Toast.makeText(Login.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }
        });



    }


}