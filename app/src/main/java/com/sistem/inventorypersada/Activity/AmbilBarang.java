package com.sistem.inventorypersada.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.reginald.editspinner.EditSpinner;
import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.Model.DataModel;
import com.sistem.inventorypersada.Model.ResponseModel;
import com.sistem.inventorypersada.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AmbilBarang extends AppCompatActivity implements View.OnClickListener {
    private EditText kondisiBrg1;
    Button scanBarcode, btnAmbil;
    private EditText barcodeId, namaBrg,typeBrg,grade;
    private String barcode,kondisi,nama_brg,type_brg,kualitas, tanggal;
    int idBarang;

    private List<DataModel> listData = new ArrayList<>();
    private SwipeRefreshLayout srlData;
    private ProgressBar progresData;
    private FloatingActionButton fabTambah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambil_barang);

        scanBarcode = findViewById(R.id.scanBarcode);
        scanBarcode.setOnClickListener(this);
        barcodeId = findViewById(R.id.barcodeId);
        btnAmbil = findViewById(R.id.btnAmbil);
        btnAmbil.setOnClickListener(this);

        srlData = findViewById(R.id.swapData);
        progresData = findViewById(R.id.pbData);
        fabTambah = findViewById(R.id.btnCreate);

        namaBrg = findViewById(R.id.namaBrg);
        typeBrg = findViewById(R.id.typeBrg);
        grade = findViewById(R.id.grade);


    }

    /// fungsi untuk barcode scanner
    private void scanBarcode(){
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CapturAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning Barcode");
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null){
            if (result.getContents() != null){
                barcodeId.setText(result.getContents());

                retrieveData();
            }
            else {
                Toast.makeText(this, "Tidak ada barcode", Toast.LENGTH_SHORT).show();
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    //onclick untuk barcode scanner & ambil barang
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.scanBarcode:
                scanBarcode();
                break;
            case R.id.btnAmbil:
                sendData();

                break;
        }
    }

    // fungsi untuk post data ke database melaalui API
    public void sendData(){
        APIRequestData ardData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> simpanlData = ardData.ardCreateData(barcode,kondisi,nama_brg,type_brg,kualitas,tanggal);

        simpanlData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(AmbilBarang.this, "kode : "+kode+" | Pesan : "+pesan, Toast.LENGTH_SHORT).show();

                APIRequestData apiRequestData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
                Call<ResponseModel> calll = apiRequestData.ardDeleteData(idBarang);
                calll.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        finish();
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {

                    }
                });
                finish();
            }

            // jika gagal menghubingi server
            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(AmbilBarang.this, "Gagal menghubungi server", Toast.LENGTH_SHORT).show();

            }
        });

    }


    //menampilkan data ketika user mengscan barcode
    public void retrieveData(){
        APIRequestData ardData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> tampilData = ardData.ardRetrieveData();

        tampilData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                //Toast.makeText(AmbilBarang.this, "Kode : "+kode+" | Pesan : "+pesan, Toast.LENGTH_SHORT).show();

                listData = response.body().getData();

                Log.i("TAG", "onResponse: "+barcodeId.getText().toString());

                for (DataModel item : listData){
                    if (item.getBarcode().equals(barcodeId.getText().toString())){
                        namaBrg.setText(item.getNama_brg());
                        typeBrg.setText(item.getType_brg());
                        grade.setText(item.getGrade());

                        type_brg = item.getType_brg();
                        barcode = item.getBarcode();
                        kondisi = "Keluar";
                        nama_brg = item.getNama_brg();
                        kualitas = item.getGrade();
                        tanggal = item.getTanggal();
                        idBarang = item.getId_barang();

                        Log.i("TAG", "onResponse: "+item.getNama_brg());
                    }else {
                        Log.i("TAG", "onResponse: "+item.getBarcode());

                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(AmbilBarang.this, "gagal menghubungi server"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}