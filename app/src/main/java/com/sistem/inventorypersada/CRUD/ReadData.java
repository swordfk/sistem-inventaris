package com.sistem.inventorypersada.CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.Adapter.AdapterGetData;
import com.sistem.inventorypersada.Model.DataModel;
import com.sistem.inventorypersada.Model.ResponseModel;
import com.sistem.inventorypersada.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReadData extends AppCompatActivity {

    private RecyclerView rvData;
    private AdapterGetData adData;
    private RecyclerView.LayoutManager lmData;
    private List<DataModel> listData = new ArrayList<>();
    private SwipeRefreshLayout srlData;
    private ProgressBar progresData;
    private FloatingActionButton fabTambah;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_data);

        rvData = findViewById(R.id.readData);
        srlData = findViewById(R.id.swapData);
        progresData = findViewById(R.id.pbData);
        fabTambah = findViewById(R.id.btnCreate);
        searchView = findViewById(R.id.search);

        lmData = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rvData.setLayoutManager(lmData);

        //retrieveData();
        srlData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlData.setRefreshing(true);
                retrieveData();
                srlData.setRefreshing(false);
            }
        });
        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReadData.this, CreateData.class));
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adData.getFilter().filter(newText);
                return false;
            }
        });
    }

    // update data ketika di close home
    @Override
    protected void onResume(){
        super.onResume();
        retrieveData();
    }

    //get data dari database
    public void retrieveData(){
        progresData.setVisibility(View.VISIBLE);
        APIRequestData ardData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> tampilData = ardData.ardRetrieveData();

        tampilData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                //Toast.makeText(ReadData.this, "Kode : "+kode+" | Pesan : "+pesan, Toast.LENGTH_SHORT).show();

                listData = response.body().getData();

                adData = new AdapterGetData(ReadData.this, listData);
                rvData.setAdapter(adData);
                adData.notifyDataSetChanged();

                progresData.setVisibility(View.INVISIBLE);

            }


            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(ReadData.this, "gagal menghubungi server"+t.getMessage(), Toast.LENGTH_SHORT).show();

                progresData.setVisibility(View.INVISIBLE);

            }
        });

    }



}