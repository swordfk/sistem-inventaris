package com.sistem.inventorypersada.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.reginald.editspinner.EditSpinner;
import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.Activity.CapturAct;
import com.sistem.inventorypersada.Model.ResponseModel;
import com.sistem.inventorypersada.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateData extends AppCompatActivity implements View.OnClickListener {

    private EditText kondisiBrg,namaBrg,jumlahBrg,typeBrg,tanggal, barcodeId, idBrg;
    private Button btnSimpan, scanBarcode;
    private String barcode,kondisi,nama,jumlah,type,kualitas,date;
    private Integer id_barang;
    private Spinner spinner;
    private TextView itemGrade;
    private EditSpinner kondisiBrg1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_data);

        //inisialisasi
        barcodeId = findViewById(R.id.barcodeId);
        kondisiBrg = findViewById(R.id.kondisiBrg);
        itemGrade = findViewById(R.id.itemGrade);
        namaBrg = findViewById(R.id.namaBrg);
        jumlahBrg = findViewById(R.id.jmlBarang);
        typeBrg = findViewById(R.id.typeBrg);
        tanggal = findViewById(R.id.tanggal);
        scanBarcode = findViewById(R.id.scanBarcode);
        scanBarcode.setOnClickListener(this);
        btnSimpan = findViewById(R.id.btnSimpan);

        //dropdown satatus dengan spinner
        //ambl data status dari string yang telah dibuat
        kondisiBrg1 = (EditSpinner) findViewById(R.id.kondisiBrg);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.kondisiBrg));
        kondisiBrg1.setAdapter(adapter1);
        kondisiBrg1.setEditable(false);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barcode = barcodeId.getText().toString();
                kondisi = kondisiBrg.getText().toString();
                nama = namaBrg.getText().toString();
                jumlah = jumlahBrg.getText().toString();
                type = typeBrg.getText().toString();
                date = tanggal.getText().toString();

                //kondisi form harus diisi
                if (barcode.trim().equals("")){
                    barcodeId.setError("Barcode harus terisi");
                }

                else if (kondisi.trim().equals("")) {
                    kondisiBrg.setError("Status harus terisi");
                }
                else if(nama.trim().equals("")){
                    namaBrg.setError("Nama barang harus diisi");
                }
                else if(jumlah.trim().equals("")){
                    jumlahBrg.setError("Jumlah barang harus diisi");

                }
                else if(type.trim().equals("")){
                    typeBrg.setError("Tipe dan jenis barang harus diisi");
                }
                else if(date.trim().equals("")){
                    tanggal.setError("Tanggal harus diisi");
                }
                else{
                    createData();
                }
            }
        });
    }

    private void createData(){
        APIRequestData ardData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> simpanlData = ardData.ardCreateData(barcode,kondisi,nama,type,jumlah,date);

        simpanlData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(CreateData.this, "kode : "+kode+" | Pesan : "+pesan, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(CreateData.this, "Gagal menghubungi server", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onClick(View v) {
        scanBarcode();

    }
    /// fungsi untuk barcode scanner
    private void scanBarcode(){
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CapturAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning Barcode");
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null){
            if (result.getContents() != null){
                barcodeId.setText(result.getContents());
            }
            else {
                Toast.makeText(this, "Tidak ada barcode", Toast.LENGTH_SHORT).show();
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
}