package com.sistem.inventorypersada.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.reginald.editspinner.EditSpinner;
import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.Model.ResponseModel;
import com.sistem.inventorypersada.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateData extends AppCompatActivity{

    private EditText kondisiBrg,namaBrg,jumlahBrg,typeBrg,tanggal, barcodeId;
    private Button btnSimpan;
    private String kondisi, barcode,nama,jumlah,type,kualitas,date;
    private Integer id_barang;
    private Spinner spinner;
    private TextView itemGrade;
    private EditSpinner kondisiBrg1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_data);

        //inisialisasi
        kondisiBrg = findViewById(R.id.kondisiBrg);
        barcodeId = findViewById(R.id.barcodeId);
        itemGrade = findViewById(R.id.itemGrade);
        namaBrg = findViewById(R.id.namaBrg);
        jumlahBrg = findViewById(R.id.jmlBarang);
        typeBrg = findViewById(R.id.typeBrg);
//        grade = findViewById(R.id.grade);
        tanggal = findViewById(R.id.tanggal);
        btnSimpan = findViewById(R.id.btnSimpan);
//        scanBarcode = findViewById(R.id.scanBarcode);
//        scanBarcode.setOnClickListener(this);


        Intent i = getIntent();
        int id = i.getIntExtra("id_barang",0);
        String brcd = i.getStringExtra("barcode");
        String knds = i.getStringExtra("kondisi");
        String  nm_brg = i.getStringExtra("nama");
        String jm_brg = i.getStringExtra("jumlah");
        String tp = i.getStringExtra("type");
//        String grd = i.getStringExtra("grade");
        String tgl = i.getStringExtra("tanggal");

        //id_barang.setText(Integer.valueOf(id));
        //idBrg.setText(Integer.valueOf(id));
        barcodeId.setText(String.valueOf(brcd));
        kondisiBrg.setText(String.valueOf(knds));
        namaBrg.setText(String.valueOf(nm_brg));
        jumlahBrg.setText(String.valueOf(jm_brg));
        typeBrg.setText(String.valueOf(tp));
//        grade.setText(String.valueOf(grd));
        tanggal.setText(String.valueOf(tgl));

//
//        grade = (EditSpinner) findViewById(R.id.grade);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
//                getResources().getStringArray(R.array.grade));
//        grade.setAdapter(adapter);
//        grade.setEditable(false);

        kondisiBrg1 = (EditSpinner) findViewById(R.id.kondisiBrg);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.kondisiBrg));
        kondisiBrg1.setAdapter(adapter1);
        kondisiBrg1.setEditable(false);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get data barang yang akan diupdate berdasarkan id barang
                //id_barang = idBrg.getId();
                id_barang = id;
                barcode = barcodeId.getText().toString();
                kondisi = kondisiBrg.getText().toString();
                nama = namaBrg.getText().toString();
                jumlah = jumlahBrg.getText().toString();
                type = typeBrg.getText().toString();
//                kualitas = grade.getText().toString();
                date = tanggal.getText().toString();

                if (barcode.trim().equals("")){
                    barcodeId.setError("Barcode harus terisi");
                }

                if (kondisi.trim().equals("")) {
                    kondisiBrg.setError("Status harus terisi");
                }

                if(nama.trim().equals("")){
                    namaBrg.setError("Nama barang harus diisi");
                }
                else if(jumlah.trim().equals("")){
                    jumlahBrg.setError("Jumlah barang harus diisi");

                }
                else if(type.trim().equals("")){
                    typeBrg.setError("Tipe dan jenis barang harus diisi");
                }
//                else if(kualitas.trim().equals("")){
//                    grade.setError("Grade barang harus diisi");
//                }
                else if(date.trim().equals("")){
                    tanggal.setError("Tanggal harus diisi");
                }
                else{
                    updateData();
                }
            }
        });
    }

    //post data kedatabase
    private void updateData(){
        APIRequestData ardData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> simpanlData = ardData.ardUpdateData(id_barang, barcode,kondisi,nama,type,jumlah,date);

        simpanlData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(UpdateData.this, "kode : "+kode+" | Pesan : "+pesan, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(UpdateData.this, "Gagal menghubungi server", Toast.LENGTH_SHORT).show();

            }
        });

    }
//    /// fungsi untuk barcode scanner
//    private void scanBarcode(){
//        IntentIntegrator integrator = new IntentIntegrator(this);
//        integrator.setCaptureActivity(CapturAct.class);
//        integrator.setOrientationLocked(false);
//        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
//        integrator.setPrompt("Scanning Barcode");
//        integrator.initiateScan();
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data){
//        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//        if (result != null){
//            if (result.getContents() != null){
//                barcodeId.setText(result.getContents());
//            }
//            else {
//                Toast.makeText(this, "Tidak ada barcode", Toast.LENGTH_SHORT).show();
//            }
//        }else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//
//    }
}