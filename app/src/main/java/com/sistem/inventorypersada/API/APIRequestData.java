package com.sistem.inventorypersada.API;

import com.sistem.inventorypersada.Model.ResponseModel;
import com.sistem.inventorypersada.Model.login.Login;
import com.sistem.inventorypersada.Model.register.Register;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIRequestData {

    //view data dari database
    @GET("retrieve.php")
    Call<ResponseModel> ardRetrieveData();

    //create data
    @FormUrlEncoded
    @POST("create.php")
    Call<ResponseModel> ardCreateData(
            @Field("barcode") String barcode,
            @Field("kondisi") String kondisi,
            @Field("nama_brg") String nama_brg,
            @Field("type_brg") String type_brg,
            @Field("grade") String grade,
            @Field("tanggal") String tanggal
    );

    //delete data
    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> ardDeleteData(
            @Field("id_barang") int id_barang
    );

    //update data
    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> ardUpdateData(
            @Field("id_barang") Integer id_barang,
            @Field("barcode") String barcode,
            @Field("kondisi") String kondisi,
            @Field("nama_brg") String nama_brg,
            //@Field("jumlah_brg") String jumlah_brg,
            @Field("type_brg") String type_brg,
            @Field("grade") String grade,
            @Field("tanggal") String tanggal
    );

    //login
    @FormUrlEncoded
    @POST("login.php")
    Call<Login> loginResponse(
      @Field("email") String email,
      @Field("password") String password
    );

    //register
    @FormUrlEncoded
    @POST("register.php")
    Call<Register> registerResponse(
            @Field("nama") String nama,
            @Field("email") String email,
            @Field("password") String password

    );

    //view barang keluar
    @GET("dataOut.php")
    Call<ResponseModel> barangKeluar(
//            @Field("barcode") String barcode,
//            @Field("nama_brg") String nama_brg,
//            @Field("type_brg") String type_brg,
//            @Field("grade") String grade
    );

    //ambil barang
    @FormUrlEncoded
    @POST("barangOut.php")
    Call<ResponseModel> barangOut(
            @Field("barcode") String barcode,
            @Field("kondisi") String kondisi,
            @Field("nama_brg") String nama_brg,
            @Field("type_brg") String type_brg,
            @Field("grade") String grade,
            @Field("tanggal") String tanggal

    );


//    Call<ResponseModel> barangOut();
}
