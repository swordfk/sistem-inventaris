package com.sistem.inventorypersada.Model.login;

import com.google.gson.annotations.SerializedName;

public class Login{

	public DataLogin getData() {
		return data;
	}

	public void setData(DataLogin data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@SerializedName("data")
	private DataLogin data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	@SerializedName("success")
	private String success;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}
}