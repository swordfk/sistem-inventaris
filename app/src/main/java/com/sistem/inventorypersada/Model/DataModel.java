package com.sistem.inventorypersada.Model;

public class DataModel {
    private int id_barang;
    private String barcode;
    private String kondisi;
    private String nama_brg;
    //private String jumlah_brg;
    private String type_brg;
    private String grade;
    private String tanggal;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getNama_brg() {
        return nama_brg;
    }

    public void setNama_brg(String nama_brg) {
        this.nama_brg = nama_brg;
    }

//    public String getJumlah_brg() {
//        return jumlah_brg;
//    }
//
//    public void setJumlah_brg(String jumlah_brg) {
//        this.jumlah_brg = jumlah_brg;
//    }

    public String getType_brg() { return type_brg; }

    public void setType_brg(String type_brg) { this.type_brg = type_brg; }

    public String getGrade() { return grade; }

    public void setGrade(String grade) { this.grade = grade; }

    public String getTanggal() { return tanggal; }

    public void setTanggal(String tanggal) { this.tanggal = tanggal; }

    public int getId_barang() { return id_barang; }

    public void setId_barang(int id_barang) { this.id_barang = id_barang; }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }
}
