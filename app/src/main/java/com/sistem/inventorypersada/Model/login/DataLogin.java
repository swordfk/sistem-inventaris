package com.sistem.inventorypersada.Model.login;

import com.google.gson.annotations.SerializedName;

public class DataLogin {
    @SerializedName("user_id")
    private String userId;

//    @SerializedName("password")
//    private int password;

    @SerializedName("nama")
    private String nama;

    @SerializedName("email")
    private String email;

    public void setUserId(String userId){
        this.userId = userId;
    }

    public String getUserId(){
        return userId;
    }

//    public int getPassword() { return password; }
//
//    public void setPassword(int password) { this.password = password; }

    public String getNama() { return nama; }

    public void setNama(String nama) { this.nama = nama; }

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

}
