package com.sistem.inventorypersada.Model.register;

import com.google.gson.annotations.SerializedName;

public class DataRegister {
    @SerializedName("nama")
    private String nama;

    @SerializedName("email")
    private String email;

    public String getNama() { return nama; }

    public void setNama(String nama) { this.nama = nama; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }
}
