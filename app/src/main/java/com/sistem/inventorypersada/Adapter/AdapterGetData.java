package com.sistem.inventorypersada.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sistem.inventorypersada.API.APIRequestData;
import com.sistem.inventorypersada.API.RetrofitServer;
import com.sistem.inventorypersada.CRUD.ReadData;
import com.sistem.inventorypersada.CRUD.UpdateData;
import com.sistem.inventorypersada.Model.DataModel;
import com.sistem.inventorypersada.Model.ResponseModel;
import com.sistem.inventorypersada.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterGetData extends RecyclerView.Adapter<AdapterGetData.HolderData> implements Filterable {
    private Context ctx;
    private List<DataModel> listBarang=new ArrayList<>();
    private int idBarang;
    List<DataModel> listDataFull;

    public AdapterGetData(Context ctx, List<DataModel> listBarang) {
        this.ctx = ctx;
        this.listBarang = listBarang;
        listDataFull = new ArrayList<>(listBarang);
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item,parent,false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    //fungsi hapus data barang
    private void deleteData(int id){
        APIRequestData ardData = RetrofitServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseModel> hapusData = ardData.ardDeleteData(id);
        Log.i("TAG", "deleteDatad: "+idBarang);
        hapusData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(ctx, "kode : "+kode+" | Pesan : "+pesan, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(ctx, "Gagal menghubungi server", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        DataModel datamodel = listBarang.get(position);

        holder.idCardView.setText(String.valueOf(datamodel.getId_barang()));
        holder.barcode.setText(datamodel.getBarcode());
        holder.kondisi.setText(datamodel.getKondisi());
        holder.namaBrg.setText(datamodel.getNama_brg());
        //holder.jumlahBrg.setText(datamodel.getJumlah_brg());
        holder.typeBrg.setText(datamodel.getType_brg());
        holder.grade.setText(datamodel.getGrade());
        holder.tanggal.setText(datamodel.getTanggal());

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            //pop up pilihan update dan hapus
            public boolean onLongClick(View v) {
                AlertDialog.Builder dialogPesan = new AlertDialog.Builder(ctx);
                //pop up perintah
                dialogPesan.setMessage("Pilih Operasi Yang Akan Dilakukan");
                dialogPesan.setCancelable(true);


                dialogPesan.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteData(datamodel.getId_barang());
                        dialogInterface.dismiss();
                        ((ReadData) ctx).retrieveData();

                    }
                });

                dialogPesan.setNegativeButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(ctx,UpdateData.class);
                        intent.putExtra("id_barang",datamodel.getId_barang());
                        intent.putExtra("barcode",datamodel.getBarcode());
                        intent.putExtra("kondisi",datamodel.getKondisi());
                        intent.putExtra("nama",datamodel.getNama_brg());
                        //intent.putExtra("jumlah",datamodel.getJumlah_brg());
                        intent.putExtra("type",datamodel.getType_brg());
                        intent.putExtra("jumlah",datamodel.getGrade());
                        intent.putExtra("tanggal",datamodel.getTanggal());

                        ctx.startActivity(intent);
                    }
                });
                dialogPesan.show();

                return false;
            }
        });

    }

    @Override
    public int getItemCount() { return listBarang.size(); }

    //fungsi searching by name
    @Override
    public Filter getFilter() {
        return searching;
    }

    private Filter searching = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DataModel>filterList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0){
                filterList.addAll(listDataFull);
            }else
            {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (DataModel item: listDataFull){
                    if (item.getNama_brg().toLowerCase().contains(filterPattern)){
                        filterList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filterList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listBarang.clear();
            listBarang.addAll((List)results.values);
            notifyDataSetChanged();

        }
    };

    //post data kedalam recyclerview
    public class HolderData extends RecyclerView.ViewHolder{
        TextView idCardView,idBrg,barcode,kondisi,namaBrg,jumlahBrg,typeBrg,grade,tanggal;
        CardView cardView;

        public HolderData(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            idCardView = itemView.findViewById(R.id.idCardView);
            barcode = itemView.findViewById(R.id.barcodeId);
            namaBrg = itemView.findViewById(R.id.namaBrg);
            kondisi = itemView.findViewById(R.id.kondisiBrg);
            //jumlahBrg = itemView.findViewById(R.id.jumlahBrg);
            typeBrg = itemView.findViewById(R.id.typeBrg);
            grade = itemView.findViewById(R.id.grade);
            tanggal= itemView.findViewById(R.id.tanggal);

//            itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//
//                    AlertDialog.Builder dialogPesan = new AlertDialog.Builder(ctx);
//                    dialogPesan.setMessage("Pilih operasi yang diinginkan");
//
//                    return false;
//                }
//            });


        }

    }
}
