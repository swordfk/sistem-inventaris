<?php
require("koneksi.php");
$perintah = "SELECT * FROM barang WHERE kondisi='out'";
$eksekusi = mysqli_query($konek, $perintah);
$cek = mysqli_affected_rows($konek);

if($cek > 0){
    $response["kode"] = 1;
    $response["pesan"] = "data tersedia";
    $response["data"] = array();

    while($ambil = mysqli_fetch_object($eksekusi)){
        $tarik["id_barang"] = $ambil -> id_barang;
        $tarik["barcode"] = $ambil -> barcode;
        $tarik["kondisi"] = $ambil -> kondisi;
        $tarik["nama_brg"] = $ambil -> nama_brg;
        $tarik["type_brg"] = $ambil -> type_brg;
        $tarik["grade"] = $ambil -> grade;
        $tarik["tanggal"] = $ambil -> tanggal;

        array_push($response["data"], $tarik);
    }
}
else{
    $response["kode"] = 0;
    $response["pesan"] = "data tidak tersedia";
}

echo json_encode($response);
mysqli_close($konek);

?>