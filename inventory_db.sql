-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Jan 2021 pada 04.46
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_Admin` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `barcode` varchar(30) NOT NULL,
  `kondisi` varchar(10) NOT NULL,
  `nama_brg` varchar(30) NOT NULL,
  `type_brg` varchar(50) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `barcode`, `kondisi`, `nama_brg`, `type_brg`, `grade`, `tanggal`, `created_at`, `updated_at`) VALUES
(13, '671251672', 'out', 'barang update', 'untuk dicoba biar keren', 'Grade C', '0000-00-00', '2021-01-07 04:01:29', '2021-01-07 04:01:29'),
(14, '12345789', 'out', 'coba update', 'coba update', 'Grade C', '2020-12-01', '2021-01-07 03:54:45', '2021-01-07 03:54:45'),
(15, '865754653', 'in', 'bebas', 'bebasss', 'asdds', '2021-01-05', '0000-00-00 00:00:00', '2021-01-07 03:32:20'),
(16, '787876', 'out', 'jhvjh', 'jhvj', 'jhv', '2021-01-05', '2021-01-07 03:33:39', '2021-01-07 03:33:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `in`
--

CREATE TABLE `in` (
  `id_in` int(11) NOT NULL,
  `barcode` int(11) NOT NULL,
  `kondisi` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `out`
--

CREATE TABLE `out` (
  `id_out` int(11) NOT NULL,
  `barcode` int(11) NOT NULL,
  `kondisi` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `take`
--

CREATE TABLE `take` (
  `id_barang` int(11) NOT NULL,
  `barcode` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `nama`, `email`, `password`, `created_at`, `updated_at`) VALUES
(4, 'adminku', 'adminku@gmail.com', 'admin12345', '2021-01-02 07:39:25', '2021-01-02 07:39:25'),
(8, 'bunga', 'bunga@gmail.com', 'bunga12345', '2021-01-03 03:02:26', '2021-01-03 03:02:26'),
(9, 'edy', 'edy@gmail.com', 'edy12345', '2021-01-03 03:09:38', '2021-01-03 03:09:38'),
(11, 'admin', 'admin@gmail.com', 'admin123', '2021-01-05 13:25:35', '2021-01-05 13:25:35'),
(12, 'member', 'member@gmail.com', 'member12345', '2021-01-05 13:27:10', '2021-01-05 13:27:10'),
(13, 'ijab', 'ijab@gmail.com', 'ijab12345', '2021-01-05 13:30:38', '2021-01-05 13:30:38');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_Admin`);

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `in`
--
ALTER TABLE `in`
  ADD PRIMARY KEY (`id_in`);

--
-- Indeks untuk tabel `out`
--
ALTER TABLE `out`
  ADD PRIMARY KEY (`id_out`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_Admin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `in`
--
ALTER TABLE `in`
  MODIFY `id_in` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `out`
--
ALTER TABLE `out`
  MODIFY `id_out` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
